﻿using System;
namespace Unibo.Oop.Events
{
    public interface IEventSource<TArg>
    {
        void Bind(EventListener<TArg> eventListener);
        void Unbind(EventListener<TArg> eventListener);
        void UnbindAll();
    }
}
