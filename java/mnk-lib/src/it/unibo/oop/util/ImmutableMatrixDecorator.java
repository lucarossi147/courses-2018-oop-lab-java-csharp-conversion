package it.unibo.oop.util;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

class ImmutableMatrixDecorator<E> implements ImmutableMatrix<E> {
    private final BaseMatrix<E> decorated;

    public ImmutableMatrixDecorator(BaseMatrix<E> decorated) {
        this.decorated = Objects.requireNonNull(decorated);
    }

    @Override
    public int getColumnsSize() {
        return decorated.getColumnsSize();
    }

    @Override
    public int getRowsSize() {
        return decorated.getRowsSize();
    }

    @Override
    public int size() {
        return decorated.size();
    }

    @Override
    public int coordDiagonal(int i, int j) {
        return decorated.coordDiagonal(i, j);
    }

    @Override
    public int coordAntidiagonal(int i, int j) {
        return decorated.coordAntidiagonal(i, j);
    }

    @Override
    public int coordRow(int d, int a) {
        return decorated.coordRow(d, a);
    }

    @Override
    public int coordColumn(int d, int a) {
        return decorated.coordColumn(d, a);
    }

    @Override
    public E get(int i, int j) {
        return decorated.get(i, j);
    }

    @Override
    public E getDiagonals(int d, int a) {
        return decorated.getDiagonals(d, a);
    }

    @Override
    public boolean contains(E element) {
        return decorated.contains(element);
    }

    @Override
    public Stream<E> getRow(int i) {
        return decorated.getRow(i);
    }

    @Override
    public Stream<E> getColumn(int i) {
        return decorated.getColumn(i);
    }

    @Override
    public Stream<E> getDiagonal(int d) {
        return decorated.getDiagonal(d);
    }

    @Override
    public Stream<E> getAntidiagonal(int a) {
        return decorated.getAntidiagonal(a);
    }

    @Override
    public void forEachIndexed(BiIntObjConsumer<E> consumer) {
        decorated.forEachIndexed(consumer);
    }

    @Override
    public Object[] toArray() {
        return decorated.toArray();
    }

    @Override
    public E[] toArray(E[] ts) {
        return decorated.toArray(ts);
    }

    @Override
    public List<E> toList() {
        return decorated.toList();
    }

    @Override
    public Iterator<E> iterator() {
        return decorated.iterator();
    }
}
