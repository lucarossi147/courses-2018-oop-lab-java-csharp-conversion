package it.unibo.oop.mnk.ui.console.control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import it.unibo.oop.mnk.MNKMatch;

class MNKConsoleConsoleControlImpl implements MNKConsoleControl {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private MNKMatch model;

    @Override
    public MNKMatch getModel() {
        return model;
    }

    @Override
    public void setModel(MNKMatch model) {
        this.model = model;
    }

    @Override
    public void input() {
            try {
            	if (model != null) {
	                while(true) {
	                	final String line = reader.readLine();
	                    final String[] coords = line.split("\\s+"); // split using one or more spaces as delimiters
	                	
	                    if (line.equalsIgnoreCase("reset")) {
	                        model.reset();
	                        break;
	                    } else if (line.equals("exit")) {
	                        System.exit(0);
	                    } else if (coords.length == 2) {
	                        final String fst = coords[0];
	                        final String snd = coords[1];
	                        final int i = fst.charAt(0) - 'a';
	
	                        try {
	                            final int j = Integer.parseInt(snd) - 1;
	                            makeMove(i, j);
	                            break;
	                        } catch (Exception e)  {
	                            System.err.println(e.getMessage());
	                        }
	                    } else {
	                        System.err.println("Wrong syntax!");
	                    }
	                }
            	}
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void makeMove(int i, int j) {
        if (model != null) {
        	model.move(i, j);
        }
    }
}
